import React from 'react'
import { Helmet } from 'react-helmet'

const MetaData = ({ title }) => {
    return (
        <Helmet>
            <title>{`${title} - Books and Coffee`}</title>
        </Helmet>
    )
}

export default MetaData
